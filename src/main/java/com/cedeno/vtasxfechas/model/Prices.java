package com.cedeno.vtasxfechas.model;




import java.io.Serializable;
import java.sql.Timestamp;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Entity  
@Table(name = "prices")
@Data
public class Prices implements Serializable{

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  
  @Column(name = "brand_Id", nullable = false, length = 2)
  private Integer BranId;

  @Column(name = "ini_Date", nullable = false)
  private Timestamp iniDate;

  @Column(name = "ult_Date", nullable = false)
  private Timestamp ultDate;

  @Column(name = "price_List", nullable = false, length = 2)
  private Integer priceList;

  @Column(name = "product_Id", nullable = false, length = 8)
  private Integer productId;

  @Column(name = "priority", nullable = false, length = 1)
  private Integer priority;

  @Column(name = "price" , nullable = false, length = 8, scale = 2)
  private Integer price;

  @Column(name = "curr", nullable = false, length = 4)
  private String curr;
  
  @Column(name = "estado", nullable = false, length = 1)
  private Integer estado;

}
