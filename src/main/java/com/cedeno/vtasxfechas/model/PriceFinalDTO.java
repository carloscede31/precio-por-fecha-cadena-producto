package com.cedeno.vtasxfechas.model;

import java.sql.Timestamp;
import jakarta.persistence.Column;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PriceFinalDTO  {


  @Column(name = "id_product")
  private Integer idProduct;
  
  @Column(name = "id_cadena")  //  Id Cadena = brand_Id  =
  private Integer idCadena;
  
  @Column(name = "tarifa")  // tarifa = price_List
  private Integer tarifa;
  
  @Column(name = "date_Aplic")
  private Timestamp dateAplic;
  
  @Column(name = "price_final")
  private Integer priceFinal;
  

}
