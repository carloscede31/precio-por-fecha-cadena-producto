package com.cedeno.vtasxfechas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VtasXfechasApplication {

	public static void main(String[] args) {
		SpringApplication.run(VtasXfechasApplication.class, args);
	}

}
