package com.cedeno.vtasxfechas.controller;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cedeno.vtasxfechas.model.PriceFinalDTO;
import com.cedeno.vtasxfechas.model.Prices;
import com.cedeno.vtasxfechas.service.IPrecioFinalService;

@RequestMapping("/preciofinal")
@RestController
public class PrecioFinalController {
  
  @Autowired
  private IPrecioFinalService service;
  
  PriceFinalDTO precioDto;
  Optional<Prices> precios;
  
     
  @PostMapping("/porproducto")
  public ResponseEntity<PriceFinalDTO> obtenerPrecio(@Validated @RequestBody PriceFinalDTO preciofin){
    precios = Optional.of(new Prices());
    precioDto = new PriceFinalDTO();
    precios = Optional.ofNullable(this.service.buscaPrecioFinal(preciofin));
    if(precios.isPresent()) {
      precioDto.setIdProduct(precios.get().getProductId());
      precioDto.setIdCadena(precios.get().getBranId());
      precioDto.setDateAplic(preciofin.getDateAplic());
      precioDto.setPriceFinal(precios.get().getPrice());
      precioDto.setTarifa(precios.get().getPriceList());
      return new ResponseEntity<PriceFinalDTO>(precioDto, HttpStatus.OK);
    }else {
      return new ResponseEntity<PriceFinalDTO>(HttpStatus.NOT_FOUND);
    }
  }

}
