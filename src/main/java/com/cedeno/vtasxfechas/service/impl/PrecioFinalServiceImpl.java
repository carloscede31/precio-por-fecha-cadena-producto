package com.cedeno.vtasxfechas.service.impl;

import java.sql.Timestamp;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cedeno.vtasxfechas.model.PriceFinalDTO;
import com.cedeno.vtasxfechas.model.Prices;
import com.cedeno.vtasxfechas.repo.IPrecioFinalRepo;
import com.cedeno.vtasxfechas.service.IPrecioFinalService;
@Service
public class PrecioFinalServiceImpl implements IPrecioFinalService{

  @Autowired
  private IPrecioFinalRepo repo;
  
  
  @Override
  public List<Prices> listar() {
    return null;
  }

  @Override
  public Prices buscaPrecioFinal(PriceFinalDTO busca) {
    Integer  idProd = busca.getIdProduct();
    Timestamp fecha = busca.getDateAplic();
    Integer cadena = busca.getIdCadena();
    return repo.buscaPrecioFinal(idProd, fecha, cadena);
  }

}
