package com.cedeno.vtasxfechas.service;

import java.util.List;

public interface ICRUD<T> {
  List<T> listar();
}
