package com.cedeno.vtasxfechas.service;

import com.cedeno.vtasxfechas.model.PriceFinalDTO;
import com.cedeno.vtasxfechas.model.Prices;

public interface IPrecioFinalService extends ICRUD<Prices>{

  Prices buscaPrecioFinal(PriceFinalDTO busca);
}
