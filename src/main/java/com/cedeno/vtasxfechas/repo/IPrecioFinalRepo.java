package com.cedeno.vtasxfechas.repo;

import java.sql.Timestamp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.cedeno.vtasxfechas.model.Prices;


public interface IPrecioFinalRepo extends JpaRepository<Prices, Long> {
 
  @Query(value = "Select * from prices p where  p.ini_date <= :fecha and p.ult_date >= :fecha "
      + " and p.product_Id = :idProd and p.brand_Id = :cadena and p.estado=1"
      + " order by p.PRIORITY DESC limit 1",nativeQuery = true)  
  
  Prices buscaPrecioFinal(
           @Param("idProd") Integer idProd, 
           @Param("fecha") Timestamp fecha,
           @Param("cadena") Integer cadena );
}
