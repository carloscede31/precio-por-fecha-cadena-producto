package com.cedeno.vtasxfechas.service.impl;

import com.cedeno.vtasxfechas.model.PriceFinalDTO;
import com.cedeno.vtasxfechas.model.Prices;
import com.cedeno.vtasxfechas.repo.IPrecioFinalRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.Timestamp;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class PrecioFinalServiceImplTest2 {


    @Mock
    private IPrecioFinalRepo repo;
    @InjectMocks
    PrecioFinalServiceImpl precioFinalService;

    private Prices precios;
    private PriceFinalDTO priceFinalDTO;
    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        precios = new Prices();
        String text = "2020-06-16 10:00:00.0";
        Timestamp p_dateAplic = Timestamp.valueOf(text);
        precios.setId(new Long(11));
        precios.setIniDate(p_dateAplic);
        precios.setPriceList(1);
        precios.setProductId(35455);
        precios.setPrice(32);
        precios.setBranId(1);
    }

    @Test
    void buscaPrecioFinal() {
        String text = "2020-06-16 21:00:00.0";
        Integer p_idProduct = 35455;
        Timestamp p_dateAplic = Timestamp.valueOf(text);
        Integer p_idCadena = 1;
        when(repo.buscaPrecioFinal(p_idProduct,
                p_dateAplic,
                p_idCadena)).thenReturn(precios);
        PriceFinalDTO priceFinalDTO = new PriceFinalDTO();

        priceFinalDTO.setDateAplic(p_dateAplic);
        priceFinalDTO.setIdCadena(1);
        priceFinalDTO.setIdProduct(35455);
        priceFinalDTO.setTarifa(1);
        priceFinalDTO.setPriceFinal(1);
        Prices preciosFinal = new Prices();
        preciosFinal = precioFinalService.buscaPrecioFinal(priceFinalDTO);
        Integer preciofin = preciosFinal.getPrice();
        assertEquals(32,  preciofin);
    }
}